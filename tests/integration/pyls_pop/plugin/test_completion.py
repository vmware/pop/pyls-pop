from pylsp import uris
from pylsp.workspace import Document

DOC_URI = uris.from_fs_path(__file__)
DOC = """
def func(hub):
    hub.mods.plugin.function(arg=1,)
"""


async def test_sub(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 11}

    ret = hub.pyls_pop.plugin.completions(config, workspace, doc, position)
    assert ret == []


async def test_nested_sub(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 15}
    ret = hub.pyls_pop.plugin.completions(config, workspace, doc, position)
    assert ret == [
        {
            "documentation": "",
            "kind": 17,
            "label": "mods.plugin",
            "sortText": "amods.plugin",
        },
        {
            "documentation": "Function doc",
            "kind": 3,
            "label": "mods.plugin.function(hub, arg1, arg2, arg3)",
            "sortText": "amods.plugin.function(hub, arg1, arg2, arg3)",
        },
    ]


async def test_function(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 28}
    ret = hub.pyls_pop.plugin.completions(config, workspace, doc, position)
    assert ret == [
        {
            "documentation": "Function doc",
            "kind": 3,
            "label": "mods.plugin.function(hub, arg1, arg2, arg3)",
            "sortText": "amods.plugin.function(hub, arg1, arg2, arg3)",
        }
    ]


async def test_signature(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 30}
    ret = hub.pyls_pop.plugin.completions(config, workspace, doc, position)
    assert ret == []
