import pathlib

from pylsp import uris
from pylsp.workspace import Document

import tests

DOC_URI = uris.from_fs_path(__file__)
DOC = """
def func(hub):
    hub.mods.plugin.function(arg=1,)
"""
p = pathlib.Path(tests.__file__)


async def test_full(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    ret = hub.pyls_pop.plugin.document_symbols(config, workspace, doc)
    assert ret == [
        {
            "container_name": None,
            "kind": 12,
            "location": {
                "range": {
                    "end": {"character": 28, "line": 2},
                    "start": {"character": 20, "line": 2},
                },
                "uri": f"file://{p.parent}/integration/pyls_pop/plugin/test_document_symbols.py",
            },
            "name": "mods.plugin.function(hub, arg1, arg2, arg3)",
        }
    ]
