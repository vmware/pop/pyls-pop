import asyncio
import pathlib
import pop.hub
import tempfile

import pytest
import tests.integration.mods

MODPATH = pathlib.Path(tests.integration.mods.__file__).parent


@pytest.fixture(scope="function")
def hub():
    h = pop.hub.Hub()
    h.pop.sub.add(dyne_name="pyls_pop")
    h.pop.sub.add(pypath="tests.integration.mods")
    yield h


@pytest.fixture(scope="function")
def temp_cache(hub):
    hub.pyls_pop.cache.EVENT.clear()
    hold = hub.pyls_pop.cache.FILE
    try:
        with tempfile.NamedTemporaryFile() as f:
            p = pathlib.Path(f.name)
            hub.pyls_pop.cache.FILE = p
            yield p
    finally:
        hub.pyls_pop.cache.FILE = hold


async def test_create(hub, temp_cache):
    await hub.pyls_pop.cache.create()
    assert temp_cache.exists()
    assert hub.pyls_pop.cache.EVENT.wait(timeout=2)
    assert hub.pyls_pop.cache.get()


async def test_full(hub, temp_cache):
    await hub.pyls_pop.cache.create()
    assert temp_cache.exists()
    assert hub.pyls_pop.cache.check()
    ret = hub.pyls_pop.cache.get()

    assert ret["mods.plugin.function"] == {
        "contracts": {
            "call": ["mods.contracts.init.call"],
            "post": ["mods.contracts.init.post"],
            "pre": ["mods.contracts.init.pre"],
        },
        "doc": "Function doc",
        "end_line_number": 6,
        "file": f"{MODPATH}/plugin.py",
        "parameters": {
            "arg1": {"annotation": "<class 'int'>"},
            "arg2": {"annotation": "<class 'bool'>"},
            "arg3": {"annotation": "<class 'str'>"},
            "hub": {},
        },
        "ref": "mods.plugin.function",
        "return_annotation": "<class 'str'>",
        "start_line_number": 1,
    }

    assert ret["mods.plugin"] == {
        "attributes": ["function"],
        "classes": {},
        "doc": "",
        "file": f"{MODPATH}/plugin.py",
        "functions": {
            "function": {
                "contracts": {
                    "call": ["mods.contracts.init.call"],
                    "post": ["mods.contracts.init.post"],
                    "pre": ["mods.contracts.init.pre"],
                },
                "doc": "Function doc",
                "end_line_number": 6,
                "file": f"{MODPATH}/plugin.py",
                "parameters": {
                    "arg1": {"annotation": "<class " "'int'>"},
                    "arg2": {"annotation": "<class " "'bool'>"},
                    "arg3": {"annotation": "<class " "'str'>"},
                    "hub": {},
                },
                "ref": "mods.plugin.function",
                "return_annotation": "<class 'str'>",
                "start_line_number": 1,
            }
        },
        "ref": "mods.plugin",
        "variables": {},
    }
