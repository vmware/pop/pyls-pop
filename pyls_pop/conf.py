# https://pop.readthedocs.io/en/latest/tutorial/quickstart.html#adding-configuration-data

CONFIG = {
    "config": {
        "default": None,
        "help": "Load extra options from a configuration file onto hub.OPT.pyls_pop",
    }
}

CLI_CONFIG = {
    "config": {"options": ["-c"]},
}

DYNE = {"pyls_pop": ["pyls_pop"]}
